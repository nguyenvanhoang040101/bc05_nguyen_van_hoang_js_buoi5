function tinhTienDien() {
  var hoTen = document.getElementById("txt-hoTen").value;
  var tongSoKw = document.getElementById("soKW").value * 1;
  var muc1 = 500,
    muc2 = 650,
    muc3 = 850,
    muc4 = 1100,
    muc5 = 1300;
  if (tongSoKw >= 0 && tongSoKw <= 50) {
    tongTien = tongSoKw * muc1;
  } else if (tongSoKw > 50 && tongSoKw <= 100) {
    tongTien = 50 * muc1 + (tongSoKw - 50) * muc2;
  } else if (tongSoKW > 100 && tongSoKW <= 200) {
    tongTien = 50 * muc1 + 50 * muc2 + (tongSoKW - 100) * muc3;
  } else if (tongSoKW > 200 && tongSoKW <= 350) {
    tongTien = 50 * muc1 + 50 * muc2 + 100 * muc3 + (tongSoKW - 200) * muc4;
  } else {
    tongTien =
      50 * muc1 + 50 * muc2 + 100 * muc3 + 150 * muc4 + (tongSoKW - 350) * muc5;
  }
  tongTien = tongTien.toLocaleString();
  document.getElementById(
    `result2`
  ).innerHTML = `Họ tên: ${hoTen}; Tiền điện: ${tongTien}`;
}
